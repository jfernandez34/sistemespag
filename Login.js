$("#Register").click(function(){
    event.preventDefault();
    $.ajax({
        method:"POST",
        url:"Register.php",
        data:{"cosa1": $("#email").val(),
            "cosa2": $("#pass").val()},
        dataType:"json",

        success:
            function (otradata){
                console.log(otradata);
                if (otradata['estat']==='OK'){
                    $("p#result").html(otradata['usuari_app']).css('color', 'green');
                } else {
                    $("p#result").html(otradata['error']+' '+otradata['usuari_app']).css('color', 'red');;
                }
                window.stop();
            },
        error:
            function (jqXHR, textStatus, error){
                console.log(jqXHR);
                alert("Error: " + textStatus + " " + error);

            }
    })

})

$("#Login").click(function(){
    event.preventDefault();
    $.ajax({
        method:"POST",
        url:"Login.php",
        data:{"cosa1": $("#email").val(),
            "cosa2": $("#pass").val()},
        dataType:"json",

        success:
            function (otradata){
                console.log(otradata);
                if (otradata['estat']==='OK'){
                    localStorage.setItem('usuari', otradata['usuari_app']);
                    $("p#result").html('Hola '+otradata['usuari_app']+' ja estàs loguejat').css('color', 'green');
                } else {
                    $("p#result").html(otradata['error']+' '+otradata['usuari_app']).css('color', 'red');;
                }
                window.stop();
            },
        error:
            function (jqXHR, textStatus, error){
                console.log(jqXHR);
                alert("Error: " + textStatus + " " + error);

            }
    })

});