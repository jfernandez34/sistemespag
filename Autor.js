const localStorageData = localStorage.getItem('usuari');
if (localStorageData) {
    $('#register').hide();
    $('header').append( '<p class="text-sm-center text-danger fw-bold">Benvingut, '+localStorage.getItem('usuari')+'</p>');
} else {
    $('#logout').hide();
    $('header').append( localStorage.getItem('usuari'));
}