function agregarFila(texto) {
    var tabla = document.getElementById('curiositats');
    var fila = tabla.insertRow();
    var textoCell = fila.insertCell(0);

    textoCell.innerHTML = texto;
}

agregarFila('Només apareix Bob Esponja en la intro de la serie.');
agregarFila('El pirata del principi no és cap personatge de la serie');
agregarFila('Bob Esponja va participar en la guerra de les aigües.');
const localStorageData = localStorage.getItem('usuari');
if (localStorageData) {
    $('#register').hide();
    $('body').append( '<p class="text-sm-center fw-bold">Benvingut, '+localStorage.getItem('usuari')+'</p>');
} else {
    $('#logout').hide();
}