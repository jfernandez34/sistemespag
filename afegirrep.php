<?php

$RandUser = randuser();
$RandSong = randsong();
$randiduser=$RandUser[0]['id'];
$randidsong=$RandSong[0]['idSongs'];

require_once("config.php");
$conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
$query = $conn->prepare("SELECT * FROM usuaris_songs where idusuari = :randiduser");
$query->bindParam("randiduser",$randiduser,PDO::PARAM_STR);
$query->execute();
$result = $query->fetch(PDO::FETCH_ASSOC);
$exist=false;
if($result != null) {
    $json = $result['reproduccions'];
    $data = json_decode($json, true);
    for ($i = 0; $i < sizeof($data); $i++) {
        if ($data[$i]["song"] == $randidsong) {
            $exist = true;
            $data[$i]["reproduccions"]++;
            $datanueva = json_encode($data);
            $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
            $query = $conn->prepare("UPDATE usuaris_songs set reproduccions = :data where idusuari = :idusuari");
            $query->bindParam("data", $datanueva, PDO::PARAM_STR);
            $query->bindParam("idusuari", $randiduser, PDO::PARAM_STR);
            $query->execute();

            $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
            $query = $conn->prepare("UPDATE songs set Reproduccions = Reproduccions+ 1, Recaptacio = Recaptacio + 2 where idSongs = :song");
            $query->bindParam("song", $randidsong, PDO::PARAM_STR);
            $query->execute();

            echo json_encode('Una visita més per part del usuari ' . $randiduser . ' a la canço amb id ' . $randidsong);
        }
    }
    if(!$exist){
        $array2=array("song"=>$randidsong,"reproduccions"=>1);
        array_push($data,$array2);
        $data=json_encode($data);
        $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
        $query = $conn->prepare("UPDATE usuaris_songs set reproduccions = :data where idusuari = :idusuari");
        $query->bindParam("data", $data, PDO::PARAM_STR);
        $query->bindParam("idusuari", $randiduser, PDO::PARAM_STR);
        $query->execute();
        echo json_encode('Afegida la canço '.$randidsong. ' a l\'usuari '.$randiduser);
    }
} else {
    $array2=array(array("song"=>$randidsong,"reproduccions"=>1));
    $array=json_encode($array2);
    $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
    $query = $conn->prepare("INSERT INTO usuaris_songs (idusuari,reproduccions) VALUES(:randiduser,:array)");
    $query->bindParam("array", $array, PDO::PARAM_STR);
    $query->bindParam("randiduser", $randiduser, PDO::PARAM_STR);
    $query->execute();
    echo json_encode('Afegit l\'usuari '.$randiduser. ' i la canço '.$randidsong);
}
function randuser()
{
    $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
    $query = $conn->prepare("SELECT id FROM usuaris_app order by rand() limit 1");
    $query->execute();
    return $query->fetchAll(PDO::FETCH_ASSOC);
}
function randsong()
{
    $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
    $query = $conn->prepare("SELECT idSongs FROM songs order by rand() limit 1");
    $query->execute();
    return $query->fetchAll(PDO::FETCH_ASSOC);
}
