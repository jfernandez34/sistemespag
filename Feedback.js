function submitForm() {
    var nombreUsuario = document.getElementById('uname').value;
    var opinion = document.getElementById('feedback').value;

    if (nombreUsuario.trim() === '' || opinion.trim() === '') {
        alert('Por favor, completa todos los campos.');
        return;
    }

    alert('¡Gracias, ' + nombreUsuario + '! Tu opinión ha sido enviada:\n\n' + opinion);
}
const localStorageData = localStorage.getItem('usuari');
if (localStorageData) {
    $('#register').hide();
    $('body').append( '<p class="text-sm-center fw-bold">Benvingut, '+localStorage.getItem('usuari')+'</p>');
} else {
    $('#logout').hide();
}