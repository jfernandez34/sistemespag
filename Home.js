$('div').css('background-color', 'cyan')
$('div').css('border-radius', '100%')
$('div').css('height', '250px')
$('div').css('width', '250px')
$('div').css('border', '1px black solid')
$('div').css('scale', '1',)
$('body').css('background-color', 'coral');
$('li:nth-child(1)').on('click', function () {$('body').css('background-color', 'Blue')});
$('li:nth-child(2)').on('click', function () {$('body').css('background-color', 'Black')});
$('li:nth-child(3)').on('click', function () {$('body').css('background-color', 'Grey')});
$('li:nth-child(4)').on('click', function () {$('body').css('background-color', 'Green')});
$('li:nth-child(5)').on('click', function () {$('body').css('background-color', 'Yellow')});
$('li:nth-child(6)').on('click', function () {$('body').css('background-color', 'Red')});
$('li:nth-child(7)').on('click', function () {$('body').css('background-color', 'Purple')});
$('li:nth-child(8)').on('click', function () {$('body').css('background-color', 'Pink')});
$('#logout').click(function(){ localStorage.removeItem('usuari') + window.location.reload()});
$('div').hover( function () {
    $(this).animate({scale: "1.2"}, 1000);
}, function () {
    $(this).animate({scale: "1.0"}, 1000);
})
const localStorageData = localStorage.getItem('usuari');
if (localStorageData) {
    $('#register').hide();
    $('header').append( '<p class="text-sm-center text-danger fw-bold">Benvingut, '+localStorage.getItem('usuari')+'</p>');
} else {
    $('#logout').hide();
    $('header').append( localStorage.getItem('usuari'));
}