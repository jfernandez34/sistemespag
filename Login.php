<?php
require_once("config.php");
$conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
$query = $conn->prepare("SELECT * FROM usuaris_app");
$query->execute();
$result = $query->fetchAll(PDO::FETCH_ASSOC);

$email = $_POST["cosa1"];
$pass = $_POST["cosa2"];
$entra=false;
$correo=false;
$contra=false;
for ($i=0; $i<sizeof($result);$i++){
    if($result[$i]['nom']==$email && $result[$i]['password']==$pass){
        $entra=true;
    } else  {
        if($result[$i]['nom']==$email && $result[$i]['password']!=$pass){
            $contra=true;
        } else {
            $correo=true;
        }
    }
}
if($entra){
    echo json_encode(array('estat'=>'OK','error'=>'','usuari_app'=>$email));
} else{
    if($contra){
        echo json_encode(array('estat'=>'KO','error'=>'Credencial no existeix','usuari_app'=>$email));
    } else {
        echo json_encode(array('estat'=>'KO','error'=>'Usuari incorrecte','usuari_app'=>$email));
    }
}
?>
