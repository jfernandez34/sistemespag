<?php
require_once("config.php");
$conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
$query = $conn->prepare("SELECT * FROM usuaris_app");
$query->execute();
$result = $query->fetchAll(PDO::FETCH_ASSOC);

$email = $_POST["cosa1"];
$pass = $_POST["cosa2"];
$entra = false;
$existe = false;
$invalid = false;
for ($i=0; $i<sizeof($result);$i++){
    if($result[$i]['nom']==$email){
        $existe=true;
    } else {
        if(!strpos($email,'@ies-sabadell.cat')){
            $invalid=true;
        } else {
            $entra=true;
        }
    }
}

if($existe){
    echo json_encode(array('estat'=>'KO','error'=>'l\'usuari existeix','usuari_app'=>$email));
} else {
    if($invalid){
        echo json_encode(array('estat'=>'KO','error'=>'correu incorrecte','usuari_app'=>$email));
    } else {
        $query = $conn->prepare("INSERT INTO usuaris_app (nom,password) values(:correu,:credencial)");
        $query->bindParam("correu", $email, PDO::PARAM_STR);
        $query->bindParam("credencial", $pass, PDO::PARAM_STR);
        $query->execute();
        echo json_encode(array('estat'=>'OK','error'=>'','usuari_app'=>$email));
    }
}
?>
