$(document).ready(function() {
    var imagen = $("img");
    var anchoOriginal = imagen.width();
    var anchoNuevo = anchoOriginal * 0.50;
    imagen.width(anchoNuevo);
});
const localStorageData = localStorage.getItem('usuari');
if (localStorageData) {
    $('#register').hide();
    $('body').append( '<p class="text-sm-center fw-bold">Benvingut, '+localStorage.getItem('usuari')+'</p>');
} else {
    $('#logout').hide();
}